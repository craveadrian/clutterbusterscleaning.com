<div id="content">
	<div class="row">
		<h1>Testimonials</h1>
		<h2 class="head1">What our customers are saying</h2>
		<p class="head2"> <img src="public/images/content/left-q.png" alt=""> You Sisters did an Excellent Job, We are Officially in Business Together 😉... <img src="public/images/content/right-q.png" alt=""></p>
		<p class="head3"> William Curry, from Fourth Dynasty Enterprise</p>
		<div class="testi">
			<dl class="">
				<dt>
					<img src="public/images/content/testi1.png" alt="Commercial">
				</dt>
				<dd>
					<h3>Commercial </h3>
					<p>We offer unique and efficient clea​ning according to your business needs. Your business activity don’t have to stop because we work according to your schedule</p>
				</dd>
			</dl>
			<dl class="">
				<dt>
					<img src="public/images/content/testi2.png" alt="Residential">
				</dt>
				<dd>
					<h3>Residential </h3>
					<p>We make sure you have a reliable cleaning professional that understands your needs so that you don’t have to worry with the stresses of cleaning and/or maintenance of your home.</p>
				</dd>
			</dl>
			<dl class="">
				<dt>
					<img src="public/images/content/testi3.png" alt="Move In/out">
				</dt>
				<dd>
					<h3>Move In/out</h3>
					<p>Why worry about the transitioning off past and future tenants. Let us come Clean and Sanitize with ease. </p>
				</dd>
			</dl>
		</div>
	</div>
</div>
