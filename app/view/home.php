<div id="content">
	<div class="row">
		<div class="content-Cont">
			<p>WELCOME TO</p>
			<h1>CLEAN <span>CLUTTER</span></h1>
			<h2>CLEANING SERVICE</h2>
			<p>As your local cleaning professional we provide a detailed Cleaning Service. You can depend on us for a thorough service whether it’s your place of business or your home. Our Service Professionals make sure “ We Use Our Time to Give You More” so you can focus on the important things in life. We offer Commercial, Janitorial, Residential, One Time Cleanings, After Party Clean-up, Move In/Move Out, Walls (no ceilings), Windows Reorganizing (uncluttering), Carpet Cleanings and Emergency Cleaning Services is also available. We also offer Environmental Responsible Products.</p>
			<a href="<?php echo URL ?>about#content" class="bttn">LEARN MORE</a>
		</div>
		<img src="public/images/content/content-img.png" alt="Content Image">
	</div>
</div>
<div id="services">
	<div class="row">
		<h2>OUR SERVICES</h2>
		<div class="servicesCont">
			<dl>
				<dt><img src="public/images/content/services-img1.png" alt="Service Image 1"></dt>
				<dd>
					<p>COMMERCIAL / RESIDENTIAL<span>CLEANING</span></p>
					<div><a href="<?php echo URL ?>services#content" class="bttn">READ MORE</a></div>
				</dd>
			</dl>
			<dl>
				<dt><img src="public/images/content/services-img2.png" alt="Service Image 2"></dt>
				<dd>
					<p>CARPET<span>CLEANING</span></p>
					<div><a href="<?php echo URL ?>services#content" class="bttn">READ MORE</a></div>
				</dd>
			</dl>
			<dl>
				<dt><img src="public/images/content/services-img3.png" alt="Service Image 3"></dt>
				<dd>
					<p>JANITORIAL<span>SERVICE</span></p>
					<div><a href="<?php echo URL ?>services#content" class="bttn">READ MORE</a></div>
				</dd>
			</dl>
			<dl>
				<dt><img src="public/images/content/services-img4.png" alt="Service Image 4"></dt>
				<dd>
					<p>BIO-HAZARD<span>CLEANING</span></p>
					<div><a href="<?php echo URL ?>services#content" class="bttn">READ MORE</a></div>
				</dd>
			</dl>
			<dl>
				<dt><img src="public/images/content/services-img5.png" alt="Service Image 5"></dt>
				<dd>
					<p>MOVE IN/OUT<span>CLEANING</span></p>
					<div><a href="<?php echo URL ?>services#content" class="bttn">READ MORE</a></div>
				</dd>
			</dl>
			<dl>
				<dt><img src="public/images/content/services-img6.png" alt="Service Image 6"></dt>
				<dd>
					<p>HOARDER<span>CLEANING</span></p>
					<div><a href="<?php echo URL ?>services#content" class="bttn">READ MORE</a></div>
				</dd>
			</dl>
		</div>
	</div>
</div>
<div id="info">
	<div class="row">
		<div class="imageCont">
			<img src="public/images/content/info-img.png" alt="Information Image">
		</div>
		<div class="infoDesc">
			<div class="Container">
				<h2>Devoted to<span>Incredible Cleaning</span></h2>
				<p>SPECIAL ALERT!!! Clutter Busters is offering 3 hours worth of Thorough Cleaning with 2 Cleaning Professionals for ONLY $150.00 (Savings of $60.00) for Residential Customers Only. Call Today to schedule your Cleaning <span><?php $this->info(["phone","tel"]);?></span></p>
				<p>We Save You Time by taking care of your cleaning needs. Some of the services we provide are: Commercial Cleaning; Move In/Out Cleaning’s for Apartments/Houses; Walls (exclude ceilings) Windows (including window panes); Uncluttering/Reorganizing Bedrooms, Basements, Garages, Offices and Kitchens; Carpet Cleaning (exclude Persian Area Rugs); One Time Cleanings and After Party Cleaning. We also offer environmental friendly products for an additional fee.</p>
			</div>
		</div>
	</div>
</div>
<div id="gallery">
	<div class="row">
		<img src="public/images/content/gallery-img1.png" alt="Gallery Image Large">
		<div class="galleryCont">
			<h2>OUR GALLERY</h2>
			<div class="imgph">
				<div class="small">
					<img src="public/images/content/gallery-img2.png" alt="Gallery Image 1">
					<img src="public/images/content/gallery-img3.png" alt="Gallery Image 2">
					<img src="public/images/content/gallery-img4.png" alt="Gallery Image 3">
					<img src="public/images/content/gallery-img5.png" alt="Gallery Image 4">
				</div>
				<img src="public/images/content/gallery-img6.png" alt="Gallery Image 5">
			</div>
			<a href="<?php echo URL ?>gallery#content" class="bttn">VIEW MORE</a>
		</div>
	</div>
</div>
<div id="tips">
	<div class="row">
		<h2>CLEANING TIPS</h2>
		<p>Hardwood Floors and Pet Stains
		<span>Make a wet paste of baking soda and white vinegar. Apply liberally to the stain and let it stand until it’s completely dry. Wipe up. Soak a clean cloth with hydrogen peroxide - the 3 percent solution you get at the drug store - and lay it on the stain. Allow it to soak up for a few seconds and BAM the stain is gone. “ per Westchester magazine”</span></p>
		<a href="<?php echo URL ?>about#content" class="bttn">READ MORE</a>
	</div>
</div>
<div id="contact">
	<div class="row">
		<h2>CONTACT US</h2>
		<form action="sendContactForm" method="post"  class="sends-email ctc-form" >
			<div class="info">
				<label><span class="ctc-hide">Name</span>
					<input type="text" name="name" placeholder="Name:">
				</label>
				<label><span class="ctc-hide">Phone</span>
					<input type="text" name="phone" placeholder="Phone:">
				</label>
				<label><span class="ctc-hide">Email</span>
					<input type="text" name="email" placeholder="Email:">
				</label>
			</div>
			<label><span class="ctc-hide">Message</span>
				<textarea name="message" cols="30" rows="10" placeholder="Message:"></textarea>
			</label>
			<div class="cb-Cont">
				<div class="conf">
					<label>
						<input type="checkbox" name="consent" class="consentBox">I hereby consent to having this website store my submitted information so that they can respond to my inquiry.
					</label><br>
					<?php if( $this->siteInfo['policy_link'] ): ?>
					<label>
						<input type="checkbox" name="termsConditions" class="termsBox"/>I hereby confirm that I have read and understood this website's <a href="<?php $this->info("policy_link"); ?>" target="_blank">Privacy Policy.</a>
					</label>
				</div>
				<div class="g-recaptcha"></div>
			</div>
			<?php endif ?>
			<button type="submit" class="ctcBtn" disabled>SUBMIT FORM</button>
		</form>
	</div>
</div>