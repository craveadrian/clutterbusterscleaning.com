<?php $this->suspensionRedirect($view); ?>
<!DOCTYPE html>
<html lang="en" <?php $this->helpers->htmlClasses(); ?>>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<meta name="apple-mobile-web-app-status-bar-style" content="black" />

	<?php $this->helpers->seo($view); ?>
	<link rel="icon" href="public/images/favicon.png" type="image/x-icon">
	<!-- <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous"> -->
	<link href="<?php echo URL; ?>public/styles/style.css" rel="stylesheet">
	<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.css" />
	<link rel="stylesheet" href="<?php echo URL; ?>public/fancybox/source/jquery.fancybox.css" media="screen" />
	<?php $this->helpers->analytics(); ?>
</head>

<body <?php $this->helpers->bodyClasses($view); ?>>
<?php $this->checkSuspensionHeader(); ?>
	<header>
		<div id="header">
			<div class="top">
				<div class="row">
					<a href="<?php echo URL ?>"><img src="./public/images/common/mainLogo-top.png" alt="Main Logo" class="mainLogo"></a>
					<nav>
						<a href="#" id="pull"><strong>MENU</strong></a>
						<ul>
							<li <?php $this->helpers->isActiveMenu("home"); ?>><a href="<?php echo URL ?>">HOME</a></li>
							<li <?php $this->helpers->isActiveMenu("about"); ?>><a href="<?php echo URL ?>about#content">ABOUT US</a></li>
							<li <?php $this->helpers->isActiveMenu("services"); ?>><a href="<?php echo URL ?>services#content">SERVICES</a></li>
						</ul>
						<ul>
							<li <?php $this->helpers->isActiveMenu("gallery"); ?>><a href="<?php echo URL ?>gallery#content">GALLERY</a></li>
							<li <?php $this->helpers->isActiveMenu("testimonials"); ?>><a href="<?php echo URL ?>testimonials#content">TESTIMONIALS</a></li>
							<li <?php $this->helpers->isActiveMenu("contact"); ?>><a href="<?php echo URL ?>contact#content">CONTACT US</a></li>
						</ul>
					</nav>
				</div>		
			</div>
			<div class="bot">
				<div class="row">
					<p class="socialMed">
						<a href="<?php $this->info("fb_link"); ?>" target="_blank"><img src="public/images/common/fb.png" alt="Facebook"></a>
						<a href="<?php $this->info("tt_link"); ?>" target="_blank"><img src="public/images/common/tw.png" alt="Twitter"></a>
						<a href="<?php $this->info("yt_link"); ?>" target="_blank"><img src="public/images/common/yt.png" alt="Youtube"></a>
						<a href="<?php $this->info("ig_link"); ?>" target="_blank"><img src="public/images/common/in.png" alt="Instagram"></a>
						<a href="<?php $this->info("gp_link"); ?>" target="_blank"><img src="public/images/common/gp.png" alt="Google Plus"></a>
					</p>
					<div class="contacts">
						<p class="call">BUSINESS<span><?php $this->info(["phone","tel"]);?></span></p>
						<p class="call">CELL<span><?php $this->info(["phone2","tel"]);?></span></p>
					</div>
				</div>
			</div>
		</div>
	</header>

	<div id="banner">
		<div class="row">
			<div class="bannerBox">
				<h2>Using Our Time to<span>Give You More</span></h2>
				<a href="<?php echo URL ?>contact#content" class="bttn">FREE ESTIMATE</a>
			</div>
		</div>
	</div>