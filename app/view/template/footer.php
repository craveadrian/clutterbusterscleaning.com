<footer>
	<div id="footer">
		<div class="top">
			<div class="row">
				<div class="cont">
					<a href="<?php echo URL ?>"><img src="./public/images/common/mainLogo-top.png" alt="Main Logo" class="mainLogo"></a>
					<p class="location"><img src="public/images/common/marker-white.png" alt="Location"><?php $this->info("address");?></p>
					<div class="contactNos">
						<img src="public/images/common/phone-white.png" alt="Telephone">
						<p>BUSINESS<span><?php $this->info(["phone","tel"]);?></span></p>
						<p>CELL<span><?php $this->info(["phone2","tel"]);?></span></p>
					</div>
					<p class="email"><img src="public/images/common/mail-white.png" alt="E-mail"><span><?php $this->info(["email","mailto"]);?></span></p>
					<div class="sched">
						<img src="public/images/common/clock-white.png" alt="Schedule">
						<p>Monday through Sunday<span></span>7:30 AM - 8:00 PM</p>
					</div>
					<p class="socialMed">
						<a href="<?php $this->info("fb_link"); ?>" target="_blank"><img src="public/images/common/fb.png" alt="Facebook"></a>
						<a href="<?php $this->info("tt_link"); ?>" target="_blank"><img src="public/images/common/tw.png" alt="Twitter"></a>
						<a href="<?php $this->info("yt_link"); ?>" target="_blank"><img src="public/images/common/yt.png" alt="Youtube"></a>
						<a href="<?php $this->info("ig_link"); ?>" target="_blank"><img src="public/images/common/in.png" alt="Instagram"></a>
						<a href="<?php $this->info("gp_link"); ?>" target="_blank"><img src="public/images/common/gp.png" alt="Google Plus"></a>
					</p>
				</div>
				<div class="mapCont">
					<img src="public/images/content/map.jpg" alt="Map">
				</div>
			</div>
		</div>
		<div class="bot">
			<div class="row">
				<p class="copy">
					Copyright © <?php echo date("Y"); ?>. <?php $this->info("company_name"); ?> All Rights Reserved. 
					<?php if( $this->siteInfo['policy_link'] ): ?>
						<a href="<?php $this->info("policy_link"); ?>">Privacy Policy</a>. 
					<?php endif ?>
				</p>
				<p class="silver"><img src="public/images/scnt.png" alt="" class="company-logo" /><a href="https://silverconnectwebdesign.com/website-development" rel="external" target="_blank">Web Design</a> Done by <a href="https://silverconnectwebdesign.com" rel="external" target="_blank">Silver Connect Web Design</a></p>
			</div>
		</div>
	</div>
</footer>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="<?php echo URL; ?>public/scripts/sendform.js" data-view="<?php echo $view; ?>" id="sendform"></script>
<!-- <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>  -->
<script src="<?php echo URL; ?>public/scripts/responsive-menu.js"></script>
<script src="https://unpkg.com/sweetalert2@7.20.10/dist/sweetalert2.all.js"></script>

<?php if( $this->siteInfo['cookie'] ): ?>
	<script src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.js"></script>
	<script>
	window.addEventListener("load", function(){
	window.cookieconsent.initialise({
	  "palette": {
	    "popup": {
	      "background": "#000"
	    },
	    "button": {
	      "background": "#3085d6"
	    }
	  }
	})});
	</script>
<?php endif ?>

<?php if(in_array($view,["home","contact"])): ?>
	<script src='//www.google.com/recaptcha/api.js?onload=captchaCallBack&render=explicit' async defer></script>
	<script>
		var captchaCallBack = function() {
			$('.g-recaptcha').each(function(index, el) {
				grecaptcha.render(el, {'sitekey' : '<?php $this->info("site_key");?>'});
			});
		};

		$('.consentBox').click(function () {
		    if ($(this).is(':checked')) {
		    	if($('.termsBox').length){
		    		if($('.termsBox').is(':checked')){
		        		$('.ctcBtn').removeAttr('disabled');
		        	}
		    	}else{
		        	$('.ctcBtn').removeAttr('disabled');
		    	}
		    } else {
		        $('.ctcBtn').attr('disabled', true);
		    }
		});

		$('.termsBox').click(function () {
		    if ($(this).is(':checked')) {
	    		if($('.consentBox').is(':checked')){
	        		$('.ctcBtn').removeAttr('disabled');
	        	}
		    } else {
		        $('.ctcBtn').attr('disabled', true);
		    }
		});

	</script>

<?php endif; ?>


<?php if ($view == "gallery"): ?>
	<script type="text/javascript" src="<?php echo URL; ?>public/fancybox/source/jquery.fancybox.js?v=2.1.5"></script>
	<script type="text/javascript" src="<?php echo URL; ?>public/scripts/jquery.pajinate.js"></script>
	<script>
		$('#gall1').pajinate({ num_page_links_to_display : 3, items_per_page : 10 });
		$('.fancy').fancybox({
			helpers: {
				title : {
					type : 'over'
				}
			}
		});
	</script>
<?php endif; ?>

<?php $this->checkSuspensionFooter(); ?>
</body>
</html>


