<div id="content">
	<div class="row">
		<h1>OUR SERVICES</h1>
		<p class="service-description">As your local cleaning professional we provide a detailed Cleaning Service. You can depend on us for a thorough service whether it’s your place of business or your home. Our Service Professionals make sure “ We Use Our Time to Give You More” so you can focus on the important things in life. We offer Commercial, Janitorial, Residential, One Time Cleanings, After Party Clean-up, Move In/Move Out, Walls (no ceilings), Windows Reorganizing (uncluttering), Carpet Cleanings and Emergency Cleaning Services is also available. We also offer Environmental Responsible Products. </p>

		<h2>Service Prices for Residential only. commercial, carpet, walls and windows please call for A free estimate</h2>
		<h3>Compare our service packages and find the best fit for you.</h3>
		<dl>
			<dt>
				<p class="header-1">Residential</p>
				<p class="header-2">$35 <span>Hour</span> </p>
				<p class="header-3">Best Package Offer</p>
			</dt>
			<dd>
				<p>Inside Windows</p>
				<p>Bathrooms</p>
				<p>Bedrooms</p>
				<p>Kitchen</p>
			</dd>
		</dl>
		<dl>
			<dt>
				<p class="header-1">Business (small)</p>
				<p class="header-2">$150 <span>Base</span></p>
				<p class="header-3">Basic cleaning</p>
			</dt>
			<dd>
				<p>Up to 1400 square foot</p>
			</dd>
		</dl>
		<dl>
			<dt>
				<p class="header-1">Uncluttering/Reorganizing</p>
				<p class="header-2">$75​ <span>Base</span>*</p>
				<p class="header-3">Two Hour Mininum</p>
			</dt>
			<dd>
				<p>Home/Office</p>
				<p>Garages/Basements</p>
				<p>Bedrooms</p>
				<p>Kitchen</p>
			</dd>
		</dl>
	</div>
</div>
